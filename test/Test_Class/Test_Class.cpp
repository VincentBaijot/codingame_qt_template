#include <QTest>

#include "Class.hpp"

class Test_Class : public QObject
{
    Q_OBJECT

  private slots:
      void test_foo();
};

void Test_Class::test_foo()
{
    Class test;
    test.foo();
    QVERIFY(true);
}

QTEST_APPLESS_MAIN(Test_Class)

#include "Test_Class.moc"
